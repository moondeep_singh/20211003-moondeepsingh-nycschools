Project Name : 20211003-MoondeepSingh-NYCSchools
Purpose : To demonstrate the Android skills.

Libraries Used

Third Party Libraries:
1. Butterknife - Dependency injection.
2. Retrofit - REST API calls.
3. Junit - Test cases
4. Mockito - For mocking test dependencies.
5. Espresso - For Unit testing.
6. Gson - Serialization/Deserialization.

Android Libraries:
1. Material Design - Design.
2. CardView - To display in card.
3. Constraint Layout - UI design
4. Recyclerview - Listing of data.