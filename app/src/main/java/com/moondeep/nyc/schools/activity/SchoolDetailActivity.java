package com.moondeep.nyc.schools.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.moondeep.nyc.schools.R;
import com.moondeep.nyc.schools.global.Constants;
import com.moondeep.nyc.schools.model.SchoolDetailResult;
import com.moondeep.nyc.schools.viewmodel.SchoolDetailActivityViewModel;

import java.util.Optional;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolDetailActivity extends AppCompatActivity {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvSchoolName)
    TextView tvSchoolName;

    @BindView(R.id.tvNumOfTestTakers)
    TextView tvNumOfTestTakers;

    @BindView(R.id.tvSatCriticalReadingScore)
    TextView tvSatCriticalReadingScore;

    @BindView(R.id.tvSatMathScore)
    TextView tvSatMathScore;

    @BindView(R.id.tvSatWritingScore)
    TextView tvSatWritingScore;

    SchoolDetailActivityViewModel schoolDetailActivityViewModel;

    String dbn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        dbn = getIntent().getStringExtra(Constants.DBN);
        onViewInit();
        initObservers();
        schoolDetailActivityViewModel.fetchSchoolDetail(dbn);
    }

    /**
     * initializes views
     */
    private void onViewInit() {
        // View Model
        schoolDetailActivityViewModel = new ViewModelProvider(this).get(SchoolDetailActivityViewModel.class);
        setTitle(R.string.nyc_schools);
    }

    /**
     * initialize observers
     */
    private void initObservers() {

        schoolDetailActivityViewModel.getErrorDialogLiveData().observe(this, showErrorPopUp -> {
            new AlertDialog.Builder(SchoolDetailActivity.this)
                    .setTitle(R.string.error_message)
                    .setMessage(R.string.error_details)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        });
        schoolDetailActivityViewModel.getSchoolSatScoresResultLiveData().observe(this, schoolsDetails -> {
            if (schoolsDetails != null) {
                progressBar.setVisibility(View.GONE);
                Optional<SchoolDetailResult> resultOptional = schoolsDetails.stream().filter(s -> s.getDbn().equals(dbn)).findFirst();

                if (resultOptional.isPresent()) {
                    refreshDataOnUI(resultOptional.get());
                } else {
                    Toast.makeText(SchoolDetailActivity.this, R.string.no_details_found, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void refreshDataOnUI(SchoolDetailResult result) {
        tvSchoolName.setText(result.getSchoolName());
        tvNumOfTestTakers.setText(String.format("%s%s", getString(R.string.sat_stats),result.getNumOfSatTestTakers()));
        tvSatCriticalReadingScore.setText(String.format("%s%s", getString(R.string.critical_reading_label),result.getSatCriticalReadingAvgScore()));
        tvSatMathScore.setText(String.format("%s%s", getString(R.string.math_avg),result.getSatMathAvgScore()));
        tvSatWritingScore.setText(String.format("%s%s", getString(R.string.writing_avg),result.getSatWritingAvgScore()));
    }

}
