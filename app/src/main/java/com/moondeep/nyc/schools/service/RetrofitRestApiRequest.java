package com.moondeep.nyc.schools.service;

import com.moondeep.nyc.schools.model.School;
import com.moondeep.nyc.schools.model.SchoolDetailResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitRestApiRequest {

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchoolsList();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetailResult>> getSchoolsDetails();
}
