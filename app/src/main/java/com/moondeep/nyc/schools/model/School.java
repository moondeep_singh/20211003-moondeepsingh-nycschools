package com.moondeep.nyc.schools.model;


import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("overview_paragraph")
    private String overviewParagraph;

    @SerializedName("location")
    private String location;

    @SerializedName("school_email")
    private String schoolEmail;

    @SerializedName("website")
    private String webSite;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }
}
