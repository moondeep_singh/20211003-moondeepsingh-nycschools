package com.moondeep.nyc.schools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.moondeep.nyc.schools.model.School;
import com.moondeep.nyc.schools.service.SchoolsRestApis;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    public LiveData<List<School>> schoolsList;
    public LiveData<Boolean> errorDialog;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        this.schoolsList = SchoolsRestApis.getInstance().getSchools();
        this.errorDialog = SchoolsRestApis.getInstance().getShowErrorDialog();
    }

    public void fetchSchoolsList() {
        SchoolsRestApis.getInstance().getSchoolsList();
    }

    public LiveData<List<School>> getSchoolsListLiveData() {
        return schoolsList;
    }

    public LiveData<Boolean> getErrorDialogLiveData() {
        return errorDialog;
    }
}
