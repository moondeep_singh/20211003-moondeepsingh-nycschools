package com.moondeep.nyc.schools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.moondeep.nyc.schools.model.SchoolDetailResult;
import com.moondeep.nyc.schools.service.SchoolsRestApis;

import java.util.List;

public class SchoolDetailActivityViewModel extends AndroidViewModel {

    public LiveData<Boolean> errorDialog;
    public LiveData<List<SchoolDetailResult>> schoolDetailResult;

    public SchoolDetailActivityViewModel(@NonNull Application application) {
        super(application);
        this.schoolDetailResult = SchoolsRestApis.getInstance().getSchoolDetailResult();
        this.errorDialog = SchoolsRestApis.getInstance().getShowErrorDialog();
    }

    public void fetchSchoolDetail(String dbn) {
        SchoolsRestApis.getInstance().getSchoolsDetails();
    }
    public LiveData<List<SchoolDetailResult>> getSchoolSatScoresResultLiveData() {
        return schoolDetailResult;
    }

    public LiveData<Boolean> getErrorDialogLiveData() {
        return errorDialog;
    }
}
