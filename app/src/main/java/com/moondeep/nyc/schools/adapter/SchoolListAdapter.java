package com.moondeep.nyc.schools.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.moondeep.nyc.schools.R;
import com.moondeep.nyc.schools.model.School;

import java.util.ArrayList;
import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.ViewHolder> {

    private final List<School> schools;
    private final ClickListener listener;

    public SchoolListAdapter(ArrayList<School> schools, ClickListener listener) {
        this.schools = schools;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SchoolListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.school_item, viewGroup, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListAdapter.ViewHolder viewHolder, int i) {
        School schoolDetails = schools.get(i);
        viewHolder.tvTitle.setText(schoolDetails.getSchoolName());
        viewHolder.tvDescription.setText(schoolDetails.getOverviewParagraph());
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final TextView tvDescription;
        private final ClickListener clickListener;

        public ViewHolder(@NonNull View itemView, ClickListener clickListener) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvOverView);
            this.clickListener = clickListener;
            itemView.setOnClickListener(this::onClick);
            itemView.setOnLongClickListener(this::onLongClick);
        }

        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }
}
