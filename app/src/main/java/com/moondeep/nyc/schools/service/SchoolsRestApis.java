package com.moondeep.nyc.schools.service;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.moondeep.nyc.schools.model.School;
import com.moondeep.nyc.schools.model.SchoolDetailResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolsRestApis {

    private static SchoolsRestApis schoolsRestApis;

    private static final String TAG = SchoolsRestApis.class.getSimpleName();
    private RetrofitRestApiRequest retrofitRestApiRequest;
    final MutableLiveData<List<School>> schools;
    final MutableLiveData<List<SchoolDetailResult>> schoolDetailResult;
    final MutableLiveData<Boolean> showErrorDialog;

    public SchoolsRestApis() {
        retrofitRestApiRequest = RetrofitApi.getRetrofitInstance().create(RetrofitRestApiRequest.class);
        schools = new MutableLiveData<>();
        schoolDetailResult = new MutableLiveData<>();
        showErrorDialog = new MutableLiveData<>();
    }

    public static SchoolsRestApis getInstance() {
        if (schoolsRestApis == null) {
            schoolsRestApis = new SchoolsRestApis();
        }
        return schoolsRestApis;
    }


    public void getSchoolsList() {
        retrofitRestApiRequest.getSchoolsList()
                .enqueue(new Callback<List<School>>() {
                    @Override
                    public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                        Log.d(TAG, "onResponse - " + response);

                        if (response.body() != null) {
                            schools.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<School>> call, Throwable t) {
                        schools.setValue(new ArrayList<>());
                        showErrorDialog.setValue(true);
                    }
                });
    }

    public void getSchoolsDetails() {
        retrofitRestApiRequest.getSchoolsDetails()
                .enqueue(new Callback<List<SchoolDetailResult>>() {
                    @Override
                    public void onResponse(Call<List<SchoolDetailResult>> call, Response<List<SchoolDetailResult>> response) {
                        Log.d(TAG, "onResponse - " + response);

                        if (response.body() != null) {
                            schoolDetailResult.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<SchoolDetailResult>> call, Throwable t) {
                        schoolDetailResult.setValue(new ArrayList<>());
                    }
                });
    }

    public LiveData<List<School>> getSchools() {
        return schools;
    }

    public LiveData<List<SchoolDetailResult>> getSchoolDetailResult() {
        return schoolDetailResult;
    }

    public MutableLiveData<Boolean> getShowErrorDialog() {
        return showErrorDialog;
    }
}
