package com.moondeep.nyc.schools.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.moondeep.nyc.schools.R;
import com.moondeep.nyc.schools.adapter.SchoolListAdapter;
import com.moondeep.nyc.schools.global.Constants;
import com.moondeep.nyc.schools.model.School;
import com.moondeep.nyc.schools.viewmodel.MainActivityViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.schoolsRecyclerView)
    RecyclerView schoolsRecyclerView;

    private ArrayList<School> schoolsList = new ArrayList<>();

    private MainActivityViewModel mainActivityViewModel;

    SchoolListAdapter schoolListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        onViewInit();
        initObservers();
        mainActivityViewModel.fetchSchoolsList();
    }

    /**
     * initializes views
     */
    private void onViewInit() {

        //setting up the layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        schoolsRecyclerView.setLayoutManager(layoutManager);
        schoolsRecyclerView.setHasFixedSize(true);


        schoolListAdapter = new SchoolListAdapter(schoolsList, listener);
        schoolsRecyclerView.setAdapter(schoolListAdapter);
        schoolsRecyclerView.addOnItemTouchListener(new RecyclerView.SimpleOnItemTouchListener());

        // View Model
        mainActivityViewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);

        setTitle(R.string.nyc_schools);
    }

    // adapter listener
    SchoolListAdapter.ClickListener listener = new SchoolListAdapter.ClickListener() {
        @Override
        public void onItemClick(int position, View v) {
            //Navigating to the stats SAT page
            Intent intent = new Intent(MainActivity.this, SchoolDetailActivity.class);
            intent.putExtra(Constants.DBN, schoolsList.get(position).getDbn());
            startActivity(intent);
        }

        @Override
        public void onItemLongClick(int position, View v) {

        }
    };

    /**
     * initialize observers
     */
    private void initObservers() {

        mainActivityViewModel.getErrorDialogLiveData().observe(this, showErrorPopUp -> {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.error_message)
                    .setMessage(R.string.error_details)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        });

        mainActivityViewModel.getSchoolsListLiveData().observe(this, schoolsDetails -> {
            if (schoolsDetails != null) {
                progressBar.setVisibility(View.GONE);
                this.schoolsList.addAll(schoolsDetails);
                schoolListAdapter.notifyDataSetChanged();
            }
        });
    }
}