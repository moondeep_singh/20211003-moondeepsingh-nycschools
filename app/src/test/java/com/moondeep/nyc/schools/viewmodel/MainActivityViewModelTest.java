package com.moondeep.nyc.schools.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.moondeep.nyc.schools.global.NYCSchoolApplication;
import com.moondeep.nyc.schools.service.SchoolsRestApis;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class MainActivityViewModelTest {

    @InjectMocks
    NYCSchoolApplication application;

    @Mock
    SchoolsRestApis schoolsRestApis;

    private MainActivityViewModel mainActivityViewModel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetSchools() {
        when(schoolsRestApis.getSchoolDetailResult()).thenReturn(new MutableLiveData<>());
        mainActivityViewModel = new MainActivityViewModel(application);
        assertNotNull(mainActivityViewModel.getSchoolsListLiveData());
    }

    @After
    public void tearDown() {
        application = null;
        mainActivityViewModel = null;
        schoolsRestApis = null;
    }
}
