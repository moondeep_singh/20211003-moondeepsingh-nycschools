package com.moondeep.nyc.schools.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.moondeep.nyc.schools.global.NYCSchoolApplication;
import com.moondeep.nyc.schools.model.SchoolDetailResult;
import com.moondeep.nyc.schools.service.SchoolsRestApis;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SchoolDetailsViewModelTest {

    @InjectMocks
    NYCSchoolApplication application;

    @Mock
    SchoolsRestApis schoolsRestApis;

    private SchoolDetailActivityViewModel schoolDetailActivityViewModel;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetSchools() {
        when(schoolsRestApis.getSchoolDetailResult()).thenReturn(new MutableLiveData<>());
        schoolDetailActivityViewModel = new SchoolDetailActivityViewModel(application);
        assertNotNull(schoolDetailActivityViewModel.getSchoolSatScoresResultLiveData());
    }

    @After
    public void tearDown() {
        application = null;
        schoolDetailActivityViewModel = null;
        schoolsRestApis = null;
    }
}
